package com.eng.jairo.reservadesalas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by jairo on 04/12/17.
 */

public class ExpandivelLVAjuda extends BaseExpandableListAdapter {
    private ArrayList<String> listaCategoria;
    private Map<String, ArrayList<String>> mapFilho;
    private Context contexto;

    public ExpandivelLVAjuda(Context contexto, ArrayList<String> categoria,
                             Map<String, ArrayList<String>> mapFilho
    ) {
        this.listaCategoria = categoria;
        this.mapFilho = mapFilho;
        this.contexto = contexto;
    }

    @Override
    public int getGroupCount() {
        return listaCategoria.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return mapFilho.get(listaCategoria.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return listaCategoria.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return mapFilho.get(listaCategoria.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String tituloCategoria = (String) getGroup(i);
        view = LayoutInflater.from(contexto).inflate(R.layout.activity_elv_grupo, null);
        TextView tvgrupo = (TextView) view.findViewById(R.id.tvGrupo);
        tvgrupo.setText(tituloCategoria);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        String item = (String) getChild(i, i1);
        view = LayoutInflater.from(contexto).inflate(R.layout.activity_elv_filho, null);
        TextView tvFilho = view.findViewById(R.id.tvFilho);
        tvFilho.setText(item);
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
