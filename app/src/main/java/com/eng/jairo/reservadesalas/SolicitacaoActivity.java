package com.eng.jairo.reservadesalas;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SolicitacaoActivity extends AppCompatActivity implements View.OnClickListener {
    Button lerQR;
    EditText etLocal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitacao);
        lerQR = (Button) findViewById(R.id.btnLerQr);
        etLocal = (EditText) findViewById(R.id.etLocal);
        lerQR.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnLerQr) {
            startActivity(new Intent(MediaStore.ACTION_IMAGE_CAPTURE));
            etLocal.setText("Local Lido Pelo QRcode");
        }
    }
}
