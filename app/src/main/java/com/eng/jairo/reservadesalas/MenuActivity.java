package com.eng.jairo.reservadesalas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {


    Button btnSolicitacao;
    Button btnMensagem;
    Button btnAjuda;
    Button btnConsulta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        btnSolicitacao = (Button) findViewById(R.id.btnSolicitacao);
        btnMensagem = (Button) findViewById(R.id.btnMensagem);
        btnAjuda = (Button) findViewById(R.id.btnAjuda);
        btnConsulta = (Button) findViewById(R.id.btnConsulta);


        btnSolicitacao.setOnClickListener(this);
        btnAjuda.setOnClickListener(this);
        btnConsulta.setOnClickListener(this);
        btnMensagem.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        //verifica o id do botão que está acessando e compara com o id do xml
        if (id == R.id.btnSolicitacao) {
            startActivity(new Intent(this, SolicitacaoActivity.class));
        } else if (id == R.id.btnAjuda) {
            startActivity(new Intent(this, AjudaActivity.class));
        } else if (id == R.id.btnConsulta) {
            startActivity(new Intent(this, ConsultaActivity.class));
        } else if (id == R.id.btnMensagem) {
            startActivity(new Intent(this, MensagemActivity.class));
        }
    }
}
