package com.eng.jairo.reservadesalas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AjudaActivity extends AppCompatActivity {
    private ExpandableListView expandableListView;
    private ExpandivelLVAjuda adapter;
    private ArrayList<String> pergunta;
    Map<String, ArrayList<String>> mapFilho;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajuda);

        expandableListView = findViewById(R.id.eLV);
        pergunta = new ArrayList<>();
        mapFilho = new HashMap<>();

        carregarDados();
    }

    public void carregarDados() {
        ArrayList<String> respostaP1 = new ArrayList<>();
        ArrayList<String> respostaP2 = new ArrayList<>();
        ArrayList<String> respostaP3 = new ArrayList<>();
        ArrayList<String> respostaP4 = new ArrayList<>();
        ArrayList<String> respostaP5 = new ArrayList<>();

        //perguntas
        pergunta.add("     1- Quando receberei a confirmação da reserva?");
        pergunta.add("     2- Como falo com um atendente?");
        pergunta.add("     3- quem tem preferência na reserva de salas?");
        pergunta.add("     4- Posso fazer mais de uma reserva?");
        pergunta.add("     5- qualquer um pode fazer a reserva?");

        //respostas
        respostaP1.add("Assim que nosso servidor verificar a disponibilidade da sala, caso esteja vaga, sua reserva será efetuada");
        respostaP2.add("Na tela \"Mensagem\" você poderá falar em tempo real com um de nossos atendentes, seja pra suporte, seja pra reservas.");
        respostaP3.add("A preferência é dada aquele que solicitou a reserva primeiro.");
        respostaP4.add("Sim, mas cada uma delas será analisada para verificar a disponibilidade e pode ser que nem todas sejam efetuadas.");
        respostaP5.add("Alunos, professores e outro funcionários podem fazer a reserva, basta terem feito um cadastrado no aplicativo.");

        mapFilho.put(pergunta.get(0), respostaP1);
        mapFilho.put(pergunta.get(1), respostaP2);
        mapFilho.put(pergunta.get(2), respostaP3);
        mapFilho.put(pergunta.get(3), respostaP4);
        mapFilho.put(pergunta.get(4), respostaP5);


        adapter = new ExpandivelLVAjuda(this, pergunta, mapFilho);
        expandableListView.setAdapter(adapter);
    }
}
