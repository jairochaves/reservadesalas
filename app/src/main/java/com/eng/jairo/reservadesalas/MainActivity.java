package com.eng.jairo.reservadesalas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnLogin;
    TextView tvMat;
    TextView tvMat2;
    TextView tvCursoSetor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvMat = (TextView) findViewById(R.id.tvMatricula);
        tvMat2 = (TextView) findViewById(R.id.tvMatricula2);
        tvCursoSetor = (TextView) findViewById(R.id.tvCursoSetor);
        btnLogin.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnLogin) {
            startActivity(new Intent(this, MenuActivity.class));
        }

    }

    public void mudaTexto(View view) {
        int id = view.getId();
        if (id == R.id.rbAluno) {
            tvMat.setText("Matrícula: ");
            tvCursoSetor.setText("Curso: ");
        } else if (id == R.id.rbServidor) {
            tvMat.setText("SIAPE: ");

            tvCursoSetor.setText("Setor: ");
        } else if (id == R.id.rbAluno2) {
            tvMat2.setText("Matrícula: ");
        } else if (id == R.id.rbServidor2) {
            tvMat2.setText("SIAPE: ");
        }
    }
}
