package com.eng.jairo.reservadesalas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ConsultaActivity extends AppCompatActivity implements View.OnClickListener{
    Button btVerResposta;
    Button btAvaliarPedido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);

        btVerResposta=(Button) findViewById(R.id.btnVerResposta);
        btAvaliarPedido=(Button) findViewById(R.id.btnAvaliarPedidos);
        btVerResposta.setOnClickListener(this);
        btAvaliarPedido.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnVerResposta){
            startActivity(new Intent(this,VerResultadosSolicitacaoActivity.class));
        }else if (v.getId()==R.id.btnAvaliarPedidos){
            startActivity(new Intent(this,AvaliarSolicitacaoActivity.class));
        }
    }
}
