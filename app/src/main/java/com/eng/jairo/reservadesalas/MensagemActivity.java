package com.eng.jairo.reservadesalas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class MensagemActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    EditText etMostraMensagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensagem);

        etMostraMensagem = (EditText) findViewById(R.id.tvMostraMensagem);


        Spinner spinner = (Spinner) findViewById(R.id.spAssuntos);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.assuntos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);
    }

    public void abrirEnviarMensagem(View view) {
        startActivity(new Intent(this, NovaMensagemActivity.class));
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i) {
            case 0:
                etMostraMensagem.setText("");
                break;
            case 1:
                etMostraMensagem.setText("Oi, bom dia.\nGostaria de saber como faço para solicitar a sala 2 no dia 21.");
                break;
            case 2:
                etMostraMensagem.setText("Mensagem Generica 2");
                break;
            case 3:
                etMostraMensagem.setText("Mensagem Generica 3");
                break;
            case 4:
                etMostraMensagem.setText("Mensagem Generica 4");
                break;
            default:
                etMostraMensagem.setText("Outras mensagens");
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
